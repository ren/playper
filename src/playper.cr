# TODO: Write documentation for `Playper`
# Should have almost no info, its used as a meta class for building the program like java main void etc. by shards build
# require marks what is needed to be built
# This is designed to initialize everything
# Reference for modules https://crystal-lang.org/reference/1.7/syntax_and_semantics/modules.html

# Commenting working code (u need to use flags if it doesn't work)
# require "./tryerror/*"
# require "./reader_input.cr"
#
# module Playper
#   include ReaderInput
#   include HelpTest
#
#   VERSION = "0.1.0"
#
#   # TODO: Put your code here
#   puts "Hello world!"
#   #ReaderInput.cli_input #runs cli_input from ReaderInput
#   HelpTest
# end

require "./swapper.cr"

module Playper
  include APIConnect
  include APISpotify
  VERSION = "0.1.0"

  APIConnect.requestGet
end
