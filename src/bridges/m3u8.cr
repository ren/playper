# M3U8
# Input
# swapper.cr
# Output
require "m3u8"

module ComposeM3U8
  include M3U8
  # Read Playlist
  file = File.read "spec/files/Playlist6.m3u"
  playlist = Playlist.parse(file)
  playlist.master? # => true

  # Generate playlist/stream
  playlist = Playlist.new
  playlist.items << SegmentItem.new(duration: 10.991, segment: "test_01.ts")
  playlist.to_s
end
