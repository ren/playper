# playper

Playlist converter from different formats.

Rosetta Stone for musical Playlists.
# TODO
- [ ] Build connectors to services
	- [ ] Youtube
	- [ ] Spotify
- [ ] Create list logic with metadata (in memory or write to file)
- [ ] First version is going to be terminal only no need to authentication and it maybe ephimeral meaning the playlist changed won't be stored, it will only swap or appear in a different platform
- [ ] Authentication service (not necesary)
- [ ] Database management
- [ ] GUI
- [ ] We have the Spotify JSON we can use that for playing for now
	- [ ] Convert it to Array/List
	- [ ] Save it as XSPF
	- [ ] Save it as m3u
	- [ ] Send it to Youtube
	- [ ] Send it to last.fm
	- [ ] Send it to libre.fm
	- [ ] Send it to music.brainz
	- [ ] Play with the metadata so we have a solid list
